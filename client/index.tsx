import * as React from "react";
import { render } from "react-dom";
import store from "./store/store";
import { BrowserRouter as Router } from 'react-router-dom'
import App from "./App";

import { Provider } from "react-redux"
window.hi = store;
import { fetchAgency } from "./store/AgencySlice";
window.fetchA = fetchAgency;
import { fetchAllVehicles } from "./store/VehicleSlice";
window.fetchV = fetchAllVehicles;
import "./fix_leaflet";
// all we do is win
render(
<Provider store={store}>
    <Router>
        <App />
    </Router>
</Provider>, document.getElementById("root"));
