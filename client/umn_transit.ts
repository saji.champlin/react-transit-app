// Dedicated API library for UMN bus transit system.
// Reversed from GopherTrip Android app.
// @saji.champlin (c) 2021
import axios from "axios";

const api_url = "https://api.peaktransit.com/v5/";
const key = "bf409cd5b7a87779c5fdcf3f18957e23";

const instance = axios.create({
  baseURL: api_url,
  params: {
    agencyID: 88,
    app_id: "_APP1",
    key: key,
    platform: "Android",
    package: "com.bishoppeaktech.android.umn_transit",
    passcode: "",
  },
});

export interface BusStop {
  id: number;
  code: number;
  name: string;
  pos: [number, number];
  closed: boolean;
  sheltered: boolean;
  disable_ETA: boolean;
}
export function make_stop(obj): BusStop {
  let stop = {} as BusStop;
  stop.id = obj["stopID"];
  stop.code = obj["stopCode"];
  stop.name = obj["stopName"];
  stop.pos = [obj["lat"], obj["lng"]];
  stop.closed = obj["closed"];
  stop.sheltered = obj["sheltered"];
  stop.disable_ETA = obj["disableETA"];
  return stop;
}

export interface Route {
  id: number;
  short_name: string;
  long_name: string;
  schedule_url: string;
  color: string;
  route_points: [number, number][];
  stops?: number[];
}

export function make_route(obj: any): Route {
  let route = {} as Route;
  route.id = obj["routeID"];
  route.short_name = obj["shortName"];
  route.long_name = obj["longName"];
  route.schedule_url = obj["scheduleURL"];
  route.color = obj["color"];
  let r = [];
  let strings = obj["routePoints"].split(";");
  for (let x in strings) {
    let c: number[] = strings[x].split(",").map(parseFloat);
    if (c.length == 2) {
      r.push([c[0], c[1]]);
    }
  }
  route.route_points = r;

  route.stops = obj["stopsIDs"];

  return route;
}

export interface Vehicle {
  id: number;
  route: number;
  trip_id: number;
  pos: [number, number];
  course: number;
  speed: number;
  in_service: boolean;
  name: string;
  vehicle_type: string;
  state: number;
  has_apc: boolean;
  apc_pct: number;
  next_stop: number;
  last_updated: number;
  updated: number;
}
export function make_vehicle(obj): Vehicle {
  let myVehicle = {} as Vehicle;
  myVehicle.id = obj["vehicleID"];
  myVehicle.route = obj["routeID"];
  myVehicle.trip_id = obj["tripID"];
  myVehicle.pos = [obj["lat"], obj["lng"]];

  myVehicle.speed = obj["speed"];
  myVehicle.in_service = Boolean(obj["oos"]);

  myVehicle.name = obj["vehicleName"];
  myVehicle.vehicle_type = obj["vehicleType"];

  myVehicle.state = obj["vehicleState"];

  myVehicle.has_apc = Boolean(obj["HasAPC"]);
  myVehicle.apc_pct = obj["APCPercentage"];

  myVehicle.next_stop = obj["nextStopID"];

  myVehicle.last_updated = obj["lastUpdated"];
  myVehicle.updated = obj["updated"];
  return myVehicle;
}
export interface Agency {
  id: number;
  name: string;
  stop: BusStop[];
  route: Route[];
}

export async function get_agency_info(): Promise<Agency> {
  const response = await instance.get("", {
    params: {
      controller: "agency",
      action: "data",
    },
  });
  let data = response.data.agency;
  data.stop = data.stop.map(make_stop);

  let stop_dict = {};

  for (let stop in data.stop) {
    let s = data.stop[stop];
    stop_dict[s.stop_id] = s;
  }

  data.route = data.route.map((obj: any) => make_route(obj));

  return data;
}

export async function get_route_vehicles(route_id: any): Promise<Vehicle[]> {
  const params = {
    controller: "vehicle",
    action: "list",
    routeID: route_id,
  };
  const response = await instance.get("", { params: params });
  const result = response.data.vehicle.map(make_vehicle);
  return result as Vehicle[];
}

export async function get_stop_eta(route_id: any, stop_id: number) {
  const params = {
    controller: "vehicle",
    action: "list",
    routeID: route_id,
    stopID: stop_id,
  };
  const response = await instance.get("", { params: params });

  return response.data.stop;
}

// test code:
//get_agency_info().then((data) => console.log(data))
