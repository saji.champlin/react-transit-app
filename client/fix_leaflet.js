// fixes the leaflet.js image bug with bundlers

import L from "leaflet";
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("url:leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("url:leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("url:leaflet/dist/images/marker-shadow.png"),
});
