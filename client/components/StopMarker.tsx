import * as React from "react";
import { useState, useEffect } from "react";
import { Marker, Popup } from "react-leaflet";
import { BusStop } from "../umn_transit";

export default function StopMarker(props: { stop: BusStop }) {
  const stop = props.stop;
  const [eta, setEta] = useState([]);
  return (
    <Marker position={stop.pos}>
      <Popup>
        {stop.name} <br /> {stop.closed ? "Closed" : "Open"}
      </Popup>
    </Marker>
  );
}
