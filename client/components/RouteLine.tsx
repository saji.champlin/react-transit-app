import * as React from "react";
import { Polyline } from "react-leaflet";
import { Route } from "../umn_transit";

const route_opacity: number = 0.75;

// RouteLine is a react-leaflet object that renders a umn transit route.
export default function RouteLine(props: { route: Route }) {
  const route = props.route;
  return (
    <Polyline
      key={route.id}
      positions={route.route_points}
      opacity={route_opacity}
      color={"#" + route.color}
      eventHandlers={{
        mouseover: (e) => {
          e.target.setStyle({ opacity: 1 });
        },
        mouseout: (e) => {
          e.target.setStyle({ opacity: route_opacity });
        },
      }}
    ></Polyline>
  );
}
