import * as React from 'react'

import { useAppSelector } from '../store/hooks'
import { MapContainer, TileLayer } from "react-leaflet";

import { selectAllStops } from "../store/StopSlice"
import { selectAllRoutes } from "../store/RouteSlice"
import { selectAllVehicles } from "../store/VehicleSlice"
import RouteLine from "./RouteLine";
import StopMarker from "./StopMarker";
import VehicleMarker from "./VehicleMarker"
const bounds: [[number, number], [number, number]] = [
    [45, -93.265],
    [44.96, -93.13],
];

export default function Map() {

  const stops = useAppSelector(selectAllStops).map((stop) => {
    return <StopMarker stop={stop} key={stop.id}/>
  })


  const routes = useAppSelector(selectAllRoutes).map((route) => {
    return <RouteLine route={route} key={route.id}/>
  })

  const vehicles = useAppSelector(selectAllVehicles).map((vehicle) => {
    return <VehicleMarker vehicle={vehicle} key={vehicle.id}></VehicleMarker>
  })

  return (
    <MapContainer
      center={[44.977, -93.2]}
      zoom={15}
      minZoom={13}
      maxBounds={bounds}
    >
      <TileLayer
        bounds={bounds}
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {stops}
      {routes}
      {vehicles}
    </MapContainer>
  );
}
