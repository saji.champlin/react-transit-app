import * as React from 'react'
import { Link, useRouteMatch } from 'react-router-dom'

export function HeaderLink(props) {
    let match = useRouteMatch(props.to)
    let classes = "font-sans text-xl font-semibold mx-2 p-2 text-white transition rounded-md ring-white ring-1"

    if (match != null && match.isExact) {
        classes += " ring"
    }
    // creates a link to a page and a clicky thing.
    return (
        <Link to={props.to}><h1 className={classes}>{props.children}</h1></Link>
    )
}