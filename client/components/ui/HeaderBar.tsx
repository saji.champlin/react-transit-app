import * as React from 'react'

export function HeaderBar(props) {
    const classes = "w-full h-20 flex-shrink-0 flex flex-row items-center px-3 bg-red-800"

    return (
        <div className={classes}>{props.children}</div>
    )
}