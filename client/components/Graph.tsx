import ForceGraph2d from 'react-force-graph-2d'
import * as React from 'react'
import { useAppSelector } from '../store/hooks'
import { selectAllStops } from '../store/StopSlice'
import { selectAllRoutes } from '../store/RouteSlice'
import { selectors } from '../store/StopRouteSlice'
import * as _ from 'lodash'

export default function Graph(props: any) {
    // we get the state from redux and calculate edges. 

    const stops = useAppSelector(selectAllStops, _.isEqual)
    
    const nodes = stops.map((s) => {
        return {
            id: s.id,
            name: s.name
        }
    });
    const stopRoutes = useAppSelector(selectors.selectAll, _.isEqual) // get all stoproutes

    const routeLoops = useAppSelector(selectAllRoutes, _.isEqual).map((r) => {
        // we are given a route. For this route, we will construct a list of stops.
        // get the id
        const id = r.id
        // get the stop ids associated w/ route
        // const myStops = stopRoutes.filter((sr) => sr.route_id == id).map((sr) => sr.stop_id)
        const myStops = r.stops
        const pairs = _.zip(myStops, [...myStops.slice(1), myStops[0]]).map((p) => {
            return {
                source: p[0],
                target: p[1],
                color: "#" + r.color
            }
        })
        return pairs
    })
    const links = _.flatten(routeLoops) as any

    const graph = { nodes, links }

    return <div className="flex-initial">
        <ForceGraph2d graphData={graph} linkDirectionalParticles={3}/> 
        </div>
}