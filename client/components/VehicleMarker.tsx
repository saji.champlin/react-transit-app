import * as React from 'react'

import { Marker, Popup } from "react-leaflet";
import { Vehicle } from "../umn_transit"
import { useAppSelector } from '../store/hooks'
import { selectRouteById } from "../store/RouteSlice"
export default function VehicleMarker(props: {vehicle: Vehicle}) {
    const vehicle = props.vehicle
    const route = useAppSelector((state) => selectRouteById(state, vehicle.route))
    return (
        <Marker position={vehicle.pos}>
            <Popup>
                {route.long_name} - {vehicle.speed}
            </Popup>
        </Marker>
    )
}