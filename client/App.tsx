import * as React from "react";
import { useEffect, useRef, useState } from 'react'
import { HeaderLink, HeaderBar } from './components/ui'
import Map from "./components/Map";
import { Switch, Route, Redirect } from "react-router-dom";
import { fetchAgency } from "./store/AgencySlice"
import { useAppDispatch } from "./store/hooks"
import { fetchAllVehicles } from "./store/VehicleSlice";
import Graph from "./components/Graph";
export function useInterval(callback: () => any, delay: number) {
  const savedCallback = useRef<() => any>();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    savedCallback.current();
    let id = setInterval(() => {
      savedCallback.current();
    }, delay);
    return () => clearInterval(id);
  }, [delay]);
}

export default function App() {
  const dispatch = useAppDispatch()
  useInterval(() => {
    dispatch(fetchAgency())
  }, 30000)
  useInterval(()=> {
    dispatch(fetchAllVehicles({}))
  },5000)
  return (
    <>
      <HeaderBar>
          <HeaderLink to="/">Map View</HeaderLink>
          <HeaderLink to="/graph">Graph View</HeaderLink>
      </HeaderBar>
      <Switch>
        <Route exact path="/">
          <Map />
        </Route>
        <Route path="/graph">
          <Graph/>
        </Route>
      </Switch>
    </>
  );
}
