// Slice that creates the general agency structure in redux.

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { Agency, get_agency_info } from "../umn_transit";
const fetchAgency = createAsyncThunk("agency/fetchAgency", get_agency_info);

const agencySlice = createSlice({
  name: "agency",
  initialState: {
    data: {} as Agency,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAgency.fulfilled, (state, action) => {
      // our action has executed properly, so now we can be happy.
      state.data = action.payload;
    });
  },
});

const { reducer } = agencySlice;
export { fetchAgency };
export default reducer;
