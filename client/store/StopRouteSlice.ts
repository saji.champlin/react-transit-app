// since we have a many-to-many relation between stops and routes
// we create a relational entity thing that allows for O(N) filtering
// where N is number of relations/edges in the graph.
import { createSlice, createEntityAdapter, createSelector } from "@reduxjs/toolkit";
import { fetchAgency } from "./AgencySlice";
import { BusStop } from "../umn_transit";
// define the interface we use

export interface StopRoute {
  id: any;
  route_id: number;
  stop_id: number;
}

const StopRouteAdapter = createEntityAdapter<StopRoute>();

const StopRouteSlice = createSlice({
  name: "stopRoute",
  initialState: StopRouteAdapter.getInitialState(),
  reducers: {
    addStopRoute: StopRouteAdapter.addOne,
    removeStopRoute: StopRouteAdapter.removeOne,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAgency.fulfilled, (state, action) => {
      StopRouteAdapter.removeAll(state);
      // now iterate through the routes and add a stopRoute for each stop on the route.
      const routes = action.payload.route;
      let newStopRoutes = [] as StopRoute[];
      routes.forEach((r) => {
        r.stops.forEach((stop_idx) => {
          const myStopRoute = {
            id: newStopRoutes.length,
            route_id: r.id,
            stop_id: stop_idx,
          } as StopRoute;
          newStopRoutes.push(myStopRoute);
        });
      });
      StopRouteAdapter.addMany(state, newStopRoutes);
    });
  },
});

export const selectors = StopRouteAdapter.getSelectors((state:any) => state.entities.stopRoutes)

// helper that gets all the route ids for a specific stop
export function getStopRoutes(state, stop_id: number) {
  const links = selectors.selectAll(state)
  return links.filter((sr) => sr.id == stop_id)
}
// likewise, given a route id, find all the stops.
// we could previously do this calculation directly.
export function getRouteStops(state, route_id: number) {
  const links = selectors.selectAll(state);
  return links.filter((sr) => sr.id == route_id)
}

// const findStopsForRoute = createSelector(
//   selectors.selectAll,
//   (state, props) => {

//   }
// )

export default StopRouteSlice;
