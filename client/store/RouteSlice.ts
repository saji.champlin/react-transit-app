import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
import { Route } from "../umn_transit";
import { fetchAgency } from "./AgencySlice";

const routeAdapter = createEntityAdapter<Route>();

const routeSlice = createSlice({
  name: "routes",
  initialState: routeAdapter.getInitialState({ loading: "idle" }),
  reducers: {
    updateRoutes: routeAdapter.upsertMany,
    removeRoutes: routeAdapter.removeMany,
    clearRoutes: routeAdapter.removeAll,
    // addRoute: (state, action: { payload: Route}) => {
    //     let r = action.payload
    //     state.entities[r.route_id] = r
    //     if (!state.ids.includes(r.route_id)) {
    //         // we don't have this yet so we add it here.
    //         state.ids.push(r.route_id)
    //     }
    // },
    // removeRoute: (state, action: { payload: number}) => {
    //     // remove a route given it's ID.
    //     let route_id = action.payload
    //     delete state.entities[route_id]
    //     state.ids = state.ids.filter((id) => {id == route_id})
    // }
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAgency.fulfilled, (state, action) => {
      state.loading = "idle";
      // use the routeadapter to add routes
      routeAdapter.removeAll(state);
      
      // since the "stops" information is redundant in this app (see StopRoute),
      // we delete it to keep state dry.
      // ES6 lets us do this on copy which is nice. (payload is immutable)
      const routes = action.payload.route
      routeAdapter.addMany(state, routes);
    });
  },
});

export default routeSlice;
// these are tools to get routes by Id.
export const {
  selectById: selectRouteById,
  selectIds: selectRouteIds,
  selectEntities: selectRouteEntities,
  selectAll: selectAllRoutes,
  selectTotal: selectTotalRoutes,
} = routeAdapter.getSelectors((state: any) => state.entities.routes);
