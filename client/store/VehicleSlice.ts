import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import { Vehicle, get_route_vehicles } from "../umn_transit";
import { selectRouteIds } from './RouteSlice'


const vehicleAdapter = createEntityAdapter<Vehicle>();

export const fetchVehicles = createAsyncThunk(
  "vehicles/fetchVehicles",
  async (route_id: number, thunk: any) => {
    const state = thunk.getState()
    // if (state.entities.agency.loading == "pending") {
    //   return // we are currently loading, so reject.
    // }
    const response = await get_route_vehicles(route_id);
    return response;
  }
);

export const fetchAllVehicles = createAsyncThunk(
  "vehicles/fetchAllVehicles",
  async (_:any, thunk: any) => {
    const state = thunk.getState()
    // if (state.entities.agency.loading == "pending") {
    //   return // we are currently loading, so reject.
    // }
    let allVehicles = [] as Vehicle[];
    const route_ids = selectRouteIds(thunk.getState())
    for (const i in route_ids) { // get vehicles for each route.
      const res = await get_route_vehicles(route_ids[i])
      allVehicles.push(...res);
    }
    return allVehicles;
  }
);

const vehicleSlice = createSlice({
  name: "vehicles",
  initialState: vehicleAdapter.getInitialState({ loading: "idle" }),
  reducers: {
    updateVehicles: vehicleAdapter.upsertMany,
    removeVehicles: vehicleAdapter.removeMany,
    clearVehicles: vehicleAdapter.removeAll,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchVehicles.fulfilled, (state, action) => {
      state.loading = "idle";
      vehicleAdapter.upsertMany(state, action.payload);
    });
    builder.addCase(fetchVehicles.pending, (state) => {
      state.loading = "pending";
    }),
    builder.addCase(fetchAllVehicles.fulfilled, (state, action) => {
      state.loading = "idle"
      vehicleAdapter.upsertMany(state, action.payload);
    }),
    builder.addCase(fetchAllVehicles.pending, (state) => {
      state.loading = "pending"
    })
  },
});
export const {
  selectById: selectVehicleById,
  selectIds: selectVehicleIds,
  selectEntities: selectVehicleEntities,
  selectAll: selectAllVehicles,
  selectTotal: selectTotalVehicles,
} = vehicleAdapter.getSelectors((state: any) => state.entities.vehicles);
export default vehicleSlice;
