import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import agencyReducer from "./AgencySlice";
import routeSlice from "./RouteSlice";
import stopSlice from "./StopSlice";
import vehicleSlice from "./VehicleSlice";
import StopRouteSlice from "./StopRouteSlice"

const store = configureStore({
  reducer: {
    entities: combineReducers({
      agency: agencyReducer,
      routes: routeSlice.reducer,
      stops: stopSlice.reducer,
      vehicles: vehicleSlice.reducer,
      stopRoutes: StopRouteSlice.reducer
    }),
  },
});
export default store;
export type AppDispatch = typeof store.dispatch;

export type RootState = ReturnType<typeof store.getState>
