import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";
import { BusStop } from "../umn_transit";
import { fetchAgency } from "./AgencySlice";

const stopAdapter = createEntityAdapter<BusStop>();

const stopSlice = createSlice({
  name: "stops",
  initialState: stopAdapter.getInitialState(),
  reducers: {
    updateStops: stopAdapter.upsertMany,
    removeStops: stopAdapter.removeMany,
    clearStops: stopAdapter.removeAll,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchAgency.fulfilled, (state, action) => {
      stopAdapter.upsertMany(state, action.payload.stop);
    });
  },
});

export default stopSlice;
// these are tools to get routes by Id.
export const {
  selectById: selectStopById,
  selectIds: selectStopIds,
  selectEntities: selectStopEntities,
  selectAll: selectAllStops,
  selectTotal: selectTotalStops,
} = stopAdapter.getSelectors((state: any) => state.entities.stops);
