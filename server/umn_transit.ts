import axios from "axios"

const api_url = "https://api.peaktransit.com/v5/"
const key = "bf409cd5b7a87779c5fdcf3f18957e23"

const instance = axios.create({
    'baseURL': api_url,
    'params': {
        "agencyID": 88,
        "app_id": "_APP1",
        "key": "bf409cd5b7a87779c5fdcf3f18957e23",
        "platform": "Android",
        "package": "com.bishoppeaktech.android.umn_transit",
        "passcode": "",
    }
})


class Coordinate {
    lat: number
    long: number
    constructor(lat: number, long: number) {
        this.lat = lat
        this.long = long
    }
}

class BusStop {
    stop_id: number
    code: number
    name: string
    pos: Coordinate
    closed: boolean
    sheltered: boolean
    disable_ETA: boolean
    constructor(obj) {
        this.stop_id = obj['stopID']
        this.code = obj['stopCode']
        this.name = obj['stopName']
        this.pos = new Coordinate(obj['lat'], obj['lng'])
        this.closed = obj['closed']
        this.sheltered = obj['sheltered']
        this.disable_ETA = obj['disableETA']
    }

    async get_route_eta(route: { route_id: any }){
        let stop = await get_stop_eta(route.route_id, this.stop_id)
        stop = stop[0]
        return [stop["ETA1"], stop["ETA2"]]
    }
}

class Route {
    route_id: any
    short_name: any
    long_name: any
    schedule_url: any
    color: any
    route_points: any[]
    stops: any[]
    constructor(obj: { [x: string]: any }, stops_dict: { [x: string]: any }) {
        this.route_id = obj['routeID']
        this.short_name = obj['shortName']
        this.long_name = obj['longName']
        this.schedule_url = obj['scheduleURL']
        this.color = obj['color']
        let r = []
        let strings = obj['routePoints'].split(";")
        for(let x in strings){
            let c: number[] = strings[x].split(",").map(parseFloat)
            r.push(new Coordinate(c[0], c[1]))
        }
        this.route_points = r

        this.stops = []
        let stopids = obj['stopsIDs']
        for(let x in stopids){
            this.stops.push(stops_dict[stopids[x]])
        }
    }

    async get_vehicles(){
        const res = await get_route_vehicles(this.route_id)
        return res
    }
}


class Vehicle {
    vehicle_id: number
    route: Route
    trip_id: number
    pos: Coordinate
    speed: number
    in_service: boolean
    name: string
    vehicle_type: string
    state: number
    has_apc: boolean
    apc_pct: number
    next_stop: BusStop
    last_updated: number
    updated: number
    constructor(obj: { [x: string]: any }, route_dict, stop_dict) {
        this.vehicle_id = obj['vehicleID']
        this.route = route_dict[obj['routeID']]
        this.trip_id = obj['tripID']
        this.pos = new Coordinate(obj['lat'], obj['lng'])

        this.speed = obj['speed']
        this.in_service = Boolean(obj['oos'])

        this.name = obj['vehicleName']
        this.vehicle_type = obj['vehicleType']

        this.state = obj['vehicleState']

        this.has_apc = Boolean(obj['HasAPC'])
        this.apc_pct = obj['APCPercentage']

        this.next_stop = stop_dict[obj['nextStopID']]

        this.last_updated = obj['lastUpdated']
        this.updated = obj['updated']
    }
}

async function get_agency_info() {
    const response = await instance.get("", {
        "params": {
            "controller": "agency",
            "action": "data"
        }
    })
    let data = response.data.agency
    data.stop = data.stop.map((o: any) => new BusStop(o))

    let stop_dict = {}
    
    for (let stop in data.stop) {
        let s = data.stop[stop]
        stop_dict[s.stop_id] = s
    }

    data.route = data.route.map((obj: any) => new Route(obj, stop_dict))

    return data
}

async function get_route_vehicles(route_id: any){
    const params = {
        "controller": "vehicle",
        "action": "list",
        "routeID": route_id
    }
    const response = await instance.get("",{"params": params})

    return response.data.vehicle
}

async function get_stop_eta(route_id: any, stop_id: number){
    const params = {
        "controller": "vehicle",
        "action": "list",
        "routeID": route_id,
        "stopID": stop_id
    }
    const response = await instance.get("", {"params": params})
    
    return response.data.stop
}

// test code:
get_agency_info().then((data) => console.log(data))
