"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var axios_1 = require("axios");
var api_url = "https://api.peaktransit.com/v5/";
var key = "bf409cd5b7a87779c5fdcf3f18957e23";
var instance = axios_1["default"].create({
    'baseURL': api_url,
    'params': {
        "agencyID": 88,
        "app_id": "_APP1",
        "key": "bf409cd5b7a87779c5fdcf3f18957e23",
        "platform": "Android",
        "package": "com.bishoppeaktech.android.umn_transit",
        "passcode": ""
    }
});
var Coordinate = /** @class */ (function () {
    function Coordinate(lat, long) {
        this.lat = lat;
        this.long = long;
    }
    return Coordinate;
}());
var BusStop = /** @class */ (function () {
    function BusStop(obj) {
        this.stop_id = obj['stopID'];
        this.code = obj['stopCode'];
        this.name = obj['stopName'];
        this.pos = new Coordinate(obj['lat'], obj['lng']);
        this.closed = obj['closed'];
        this.sheltered = obj['sheltered'];
        this.disable_ETA = obj['disableETA'];
    }
    BusStop.prototype.get_route_eta = function (route) {
        return __awaiter(this, void 0, void 0, function () {
            var stop;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, get_stop_eta(route.route_id, this.stop_id)];
                    case 1:
                        stop = _a.sent();
                        stop = stop[0];
                        return [2 /*return*/, [stop["ETA1"], stop["ETA2"]]];
                }
            });
        });
    };
    return BusStop;
}());
var Route = /** @class */ (function () {
    function Route(obj, stops_dict) {
        this.route_id = obj['routeID'];
        this.short_name = obj['shortName'];
        this.long_name = obj['longName'];
        this.schedule_url = obj['scheduleURL'];
        this.color = obj['color'];
        var r = [];
        var strings = obj['routePoints'].split(";");
        for (var x in strings) {
            var c = strings[x].split(",").map(parseFloat);
            r.push(new Coordinate(c[0], c[1]));
        }
        this.route_points = r;
        this.stops = [];
        var stopids = obj['stopsIDs'];
        for (var x in stopids) {
            this.stops.push(stops_dict[stopids[x]]);
        }
    }
    Route.prototype.get_vehicles = function () {
        return __awaiter(this, void 0, void 0, function () {
            var res;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, get_route_vehicles(this.route_id)];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res];
                }
            });
        });
    };
    return Route;
}());
var Vehicle = /** @class */ (function () {
    function Vehicle(obj, route_dict, stop_dict) {
        this.vehicle_id = obj['vehicleID'];
        this.route = route_dict[obj['routeID']];
        this.trip_id = obj['tripID'];
        this.pos = new Coordinate(obj['lat'], obj['lng']);
        this.speed = obj['speed'];
        this.in_service = Boolean(obj['oos']);
        this.name = obj['vehicleName'];
        this.vehicle_type = obj['vehicleType'];
        this.state = obj['vehicleState'];
        this.has_apc = Boolean(obj['HasAPC']);
        this.apc_pct = obj['APCPercentage'];
        this.next_stop = stop_dict[obj['nextStopID']];
        this.last_updated = obj['lastUpdated'];
        this.updated = obj['updated'];
    }
    return Vehicle;
}());
function get_agency_info() {
    return __awaiter(this, void 0, void 0, function () {
        var response, data, stop_dict, stop_1, s;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, instance.get("", {
                        "params": {
                            "controller": "agency",
                            "action": "data"
                        }
                    })];
                case 1:
                    response = _a.sent();
                    data = response.data.agency;
                    data.stop = data.stop.map(function (o) { return new BusStop(o); });
                    stop_dict = {};
                    for (stop_1 in data.stop) {
                        s = data.stop[stop_1];
                        stop_dict[s.stop_id] = s;
                    }
                    data.route = data.route.map(function (obj) { return new Route(obj, stop_dict); });
                    return [2 /*return*/, data];
            }
        });
    });
}
function get_route_vehicles(route_id) {
    return __awaiter(this, void 0, void 0, function () {
        var params, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    params = {
                        "controller": "vehicle",
                        "action": "list",
                        "routeID": route_id
                    };
                    return [4 /*yield*/, instance.get("", { "params": params })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response.data.vehicle];
            }
        });
    });
}
function get_stop_eta(route_id, stop_id) {
    return __awaiter(this, void 0, void 0, function () {
        var params, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    params = {
                        "controller": "vehicle",
                        "action": "list",
                        "routeID": route_id,
                        "stopID": stop_id
                    };
                    return [4 /*yield*/, instance.get("", { "params": params })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response.data.stop];
            }
        });
    });
}
// test code:
get_agency_info().then(function (data) { return console.log(data); });
